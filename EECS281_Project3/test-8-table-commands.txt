#testing join with the same table
CREATE table1 6 bool int int string double string is_funny age weight first_name height last_name
INSERT INTO table1 5 ROWS 
true 10 200 john 5.5 john
false 50 400 tim 5.4 smith
true 30 400 amy 5.7 amy
false 20 125 tina 5.3 mcdonald
true 40 130 billy 6.2 joe
JOIN table1 AND table1 WHERE first_name = last_name AND PRINT 2 first_name 1 last_name 2
JOIN table1 AND table1 WHERE weight = weight AND PRINT 2 first_name 1 weight 1
#testing insert and delete after generating index
GENERATE FOR table1 bst INDEX ON age
INSERT INTO table1 3 ROWS
true 10 200 john 5.5 john
false 100 100 bert 6.7 mills
false 30 100 donald 6.4 ken
PRINT FROM table1 2 first_name last_name WHERE age > 30
DELETE FROM table1 WHERE is_funny = false
PRINT FROM table1 2 first_name last_name WHERE age > 30
QUIT

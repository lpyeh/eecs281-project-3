 //
//  main.cpp
//  EECS281_Project3
//
//  Created by Leigh Yeh on 3/9/18.
//  Copyright © 2018 Leigh Yeh. All rights reserved.
//

#include <iostream>
#include "getopt.h"
#include "Table.h"
#include <stdio.h>
#include <string>
#include <vector>
#include <unordered_map>
#include "TableEntry.h"
#include <algorithm>

using std::cin;
using std::cout;
using std::boolalpha;

enum class Op {lessThan, greaterThan, equals};


class pred {
private:
    Op opUsed;
    TableEntry valueIn;
    int colIndex;
public:
    pred(const Op &op_in, const TableEntry &n, int &colIndex_in) : opUsed(op_in), valueIn(n), colIndex(colIndex_in) {}
    
    bool operator() (std::vector<TableEntry>const &input) {
        if (opUsed == Op::equals) {
            return valueIn == input.at(colIndex);
        }
        else if (opUsed == Op::greaterThan) {
            return input.at(colIndex) > valueIn;
        }
        else {
            return input.at(colIndex) < valueIn;
        }
    }
    
};


static std::unordered_map<std::string, Table>database;

void get_opt(int argc, char ** argv);

//"Create" function
void create();

//"Insert Into" function
void insert();

//"Delete From"
void delete_from();

//"Print"
void print();

//"Generate For"
void generate(const std::string &tableNameIn);

//Second Generate function to handle re-hashing
void generate_on_col(const std::string &tableIn, const std::string &colNameIn);

//"Join"
void join();

TableEntry returnTable(const Table::Type &typeIn);

bool tableFound(const std::string &inTableName);

void printTableError(const std::string &tableName);

bool quietMode = false;

int main(int argc, char** argv) {
    
#ifdef __APPLE__
    char* _input = getenv("_input");
    if ( _input != nullptr ) {
        if (!freopen( _input, "r", stdin)) {
            std::cerr << "Unable to open input file" << std::endl;
            exit(1);
        }
    }
#endif

    std::ios_base::sync_with_stdio(false);
    
    cin >> boolalpha;
    cout << boolalpha;

    //Get_opt to handle command line arguments
    get_opt(argc, argv);
    
    std::string command;
    
    do {
        std::string temp;
        std::string tableName;

        
        cout << "% ";
        cin >> command;
        
        //COMMENT: If first character is #, then it's a comment
        if (command.at(0) == '#') {
            getline(cin, temp);
        }
        
        //CREATE
        else if (command == "CREATE") {
            create();
        }
        
        //INSERT INTO
        else if (command == "INSERT") {
            
            //Read in the "INTO"
            cin >> temp;
            insert();
        }
        
        //REMOVE
        else if (command == "REMOVE") {
            cin >> tableName;
            std::unordered_map<std::string, Table>::const_iterator found = database.find(tableName);
            
            if (found != database.end()) {
                database.erase(found);
                cout << "Table " << tableName << " deleted" << std::endl;
            }
            else {
                cout << "Error: " << tableName << " does not name a table in the database\n";
            }
        }
        else if (command == "GENERATE") {
            std::string tableName;
            //cin the "FOR"
            cin >> temp >> tableName;
            std::unordered_map<std::string, Table>::const_iterator found = database.find(tableName);
            
            if (found == database.end()) {
                cout << "Error: " << tableName << " does not name a table in the database\n";
                //Clear out rest of line
                getline(cin, temp);
            }
            else {
                generate(tableName);
            }
        }
        //NEED TO FINISH
        else if (command == "PRINT") {
            //cin the "FROM"
            cin >> temp;
            print();
        }
        
        else if (command == "DELETE") {
            //cin the "FROM"
            cin >> temp;
            delete_from();
        }
        //Else command is JOIN
        else if (command == "JOIN") {
            join();
        }
     } while (command != "QUIT");
    
    //Handles QUIT
    if (command == "QUIT") {
        cout << "Thanks for being silly!" << std::endl;
    }

    
    return 0;
}

/******************************/
/*****HELPER FUNCTIONS*********/
/******************************/

void get_opt(int argc, char**argv) {
    int optindex = 0;
    int option = 0;
    
    opterr = false;
    
    struct option comOpts[] = { {"quiet", no_argument, nullptr, 'q'},
        {"help", no_argument, nullptr, 'h'}};
    
    while ((option = getopt_long(argc, argv, "qh", comOpts, &optindex)) != -1) {
        switch (option) {
                case 'h':
                    cout << "Help!\n";
                    break;
                case 'q':
                    quietMode = true;
                break;
        }
    }
}

//Returns TableEntry type, reduces code duplication
TableEntry returnTable(const Table::Type &typeIn) {
    switch(typeIn) {
        case (Table::Type::Bool): {
            bool x;
            cin >> x;
            return TableEntry(x);
            break;
        }
        case (Table::Type::Double): {
            char in[64];
            double x;
            cin >> in;
            x = std::strtod(in, NULL);
            return TableEntry(x);
            break;
        }
        case (Table::Type::Int): {
            int x;
            cin >> x;
            return TableEntry(x);
            break;
        }
        case (Table::Type::String): {
            std::string x;
            cin >> x;
            return TableEntry(x);
            break;
        }
    }
    return TableEntry(0);
}

//Checks to see if table is found in database
//returns TRUE if table is found
//returns FALSE if table is NOT found
bool tableFound(const std::string &inTableName) {
    std::unordered_map<std::string, Table>::const_iterator found = database.find(inTableName);
    if (found != database.end()) {
        return true;
    }
    else {
        return false;
    }
}

void printTableError(std::string &tableName) {
    cout << "Error: " << tableName << " does not name a table in the database\n";
}

/***CREATE**/
void create() {
    std::string tableName;
    int tableSize;
    std::string type;
    std::string temp;
    
    //Read in table name and N
    cin >> tableName >> tableSize;
    
    //If table name is found in database
    if (tableFound(tableName)) {
        cout << "Error: Cannot create already existing table " << tableName << std::endl;
        //Get rest of CREATE instruction
        getline(cin, temp);
        return;
    }
    
    else {
        
        //Put table into database
        database.emplace(tableName, Table(tableSize));
        database[tableName].types.reserve(tableSize);
        database[tableName].table.reserve(tableSize);
        database[tableName].colnames.reserve(tableSize);
        database[tableName].hash.reserve(tableSize);
        
        //For loop to get column types
        for (int i = 0; i < tableSize; i++) {
            
            //Read in the column types
            cin >> type;
            
            if (type == "string") {
                //Put "String" enum in back of "types" vector
                database[tableName].types.push_back(Table::Type::String);
            }
            else if (type == "bool") {
                database[tableName].types.emplace_back(Table::Type::Bool);
            }
            else if (type == "int") {
                database[tableName].types.emplace_back(Table::Type::Int);
            }
            else {
                database[tableName].types.emplace_back(Table::Type::Double);
            }
        }
        
        std::string colName;
    
        //Print out column name after reading them in
        cout << "New table " << tableName  << " with column(s) ";
    
        //For loop to get column names
        for (int i = 0; i < tableSize; i++) {
        
            //Read in column name
            cin >> colName;
        
            //Push back into database unordered_map
            database[tableName].colnames.emplace_back(colName);
            database[tableName].colIndex[colName] = i;
        
            //Print out column name with space after
            cout << colName << " ";
        }
    
        //Print out last of output for CREATE
        cout << "created" << std::endl;
    }
}

void insert() {
    std::string tableName;
    std::string temp;
    int rowSize;

    
    cin >> tableName;
    cin >> rowSize;
    //Gets "ROWS"
    cin >> temp;
    
    //If tableName is not found (iterator == end)
    if (!tableFound(tableName)) {
        printTableError(tableName);
        
        //Read in the number of rows specified by tableSize
        for (int i = 0; i < rowSize; i++) {
            getline(cin, temp);
        }
        return;
    }

    //Else tableName is found, insert into corresponding table
    else {
        std::vector<TableEntry>row;
        size_t startPos = database[tableName].table.size();
        
        cout << "Added " << rowSize << " rows to " << tableName << " from position " << startPos << " to " <<
        (rowSize + startPos - 1) << std::endl;
        
        //Reserve right number of items (aka number of columns) in temporary vector
        row.reserve(database[tableName].numCols);
        
        //Outer for handling rows (inner vector size)
        //Inner for handling the number of columns (equivalent to types size)
        for (int j = 0; j < rowSize; j++) {
            for (int i = 0; i < database[tableName].numCols; i++) {
                row.push_back(returnTable(database[tableName].types[i]));
            } //End inner for loop
            database[tableName].table.push_back(row);
            row.clear();
        } //End outer for loop
    }
    
    //If a column has a hash table associated with it
    if (database[tableName].hashCol != "") {
        //Generate a new hash for that column
        generate_on_col(tableName, database[tableName].hashCol);
        
    }
    
}


//NEED TO ADD CONDITIONAL/HASH INDEX PART
//PRINT
void print() {
    std::vector<std::string>columns;
    std::vector<int>columnIndices;
    Op opIn(Op::equals);
    std::string tableName;
    std::string all;
    std::string columnName;
    int size;
    char op;
    size_t numRows_printed = 0;
    bool colError = false;
    
    cin >> tableName;
    cin >> size;
    
    //If table name is not found in database
    if (!tableFound(tableName)) {
        std::string temp;
        printTableError(tableName);
        getline(cin, temp);
        return;
    }
    //Getting column names and the indices
    for (int i = 0; i < size; i++) {
    
        cin >> columnName;

        auto findColumn =
        std::find(database[tableName].colnames.begin(), database[tableName].colnames.end(), columnName);
    
        //Error checking for column name (if not found in colnames vector)
        
        if (findColumn != database[tableName].colnames.end()) {
            columns.push_back(columnName);
            columnIndices.push_back(database[tableName].colIndex[columnName]);
        }
        
        else {
            cout << "Error: " << columnName << " does not name a column in " << tableName << std::endl;
            //Use columnName to get the rest of the line, then break out of loop
            getline(cin, columnName);
            colError = true;
            break;
        }
    }
        
    if (colError == true) {
        return;
    }
    
    cin >> all;
    
    //FIXME: What to do when no items exist in the column?
    
    //Print column names if not quiet mode
    if (!quietMode) {
        //Print out all the column names first
        for (int i = 0; i < size; i++) {
            cout << columns[i] << " ";
        }
        cout << std::endl;
    }
        
    //If ALL is specified
    if (all == "ALL") {
        numRows_printed = database[tableName].table.size();
        //FIXME: Outside is the number of rows (so you print out a whole row
        //in the inner vector, then move down a row)
        for (size_t i = 0; i < database[tableName].table.size(); i++) {
            //Inner vector prints out across rows, so if two columns were specified, then
            //two items will be printed out before endl-ing
            if (!quietMode) {
                for (int j = 0; j < size; j++) {
                    //Want to print the column corresponding to the index (not 0, 1, 2, etc)
                    cout << database[tableName].table[i][columnIndices[j]] << " ";
                }//End inner
                cout << std::endl;
            }//End if !quietMode
        }//End outer
    }
    
    /***WHERE CONDITION***/
    if (all == "WHERE") {
        //    WHERE   columnname   =/</>
        cin >> columnName >> op;
        switch(op) {
            case '<': opIn = Op::lessThan;
                break;
            case '>': opIn = Op::greaterThan;
                break;
            case '=': opIn = Op::equals;
                break;
        }
            
        //Found vector to find column name in database (colnames) vector
        auto found =
        std::find(database[tableName].colnames.begin(), database[tableName].colnames.end(), columnName);
        
        if (found == database[tableName].colnames.end()) {
            cout << "Error: " << columnName << " does not name a column in " << tableName << std::endl;
            //Use columnName to get rest of line
            getline(cin, columnName);
            return;
        }
            
        //colIndex to get the indices of the column names in table
        int colIndex = int(std::distance(database[tableName].colnames.begin(), found));
        
        //"condition" returned as TableEntry object
        TableEntry condition(returnTable(database[tableName].types[colIndex]));

        
        /***BST***/
        if (!database[tableName].bst.empty() &&
            database[tableName].hashCol == columnName) {

            //If operator "="
            if (opIn == Op::equals) {
                //Find the item in BST
                auto foundCondition = database[tableName].bst.find(condition);
                    
                //EQUALS
                if (foundCondition != database[tableName].bst.end()) {
                    for (size_t i = 0; i < foundCondition->second.size(); i++) {
                        //Else just keep track of numRows_printed
                        ++numRows_printed;
                        if (!quietMode) {
                            for (int j = 0; j < size; j++) {
                                cout << database[tableName].table[foundCondition->second[i]][columnIndices[j]] <<
                                " ";
                            }//End inner for
                        //If it's not quietmode, cout endline
                        cout << "\n";
                        }
                    }//End outer for
                }
            }
            
            //LESS THAN
            else if (opIn == Op::lessThan) {
                //Lower_bound iterator
                auto low = database[tableName].bst.lower_bound(condition);
                //Iterate through all the TableEntry's less than condition
                for (auto i = database[tableName].bst.begin(); i != low; i++) {
                    numRows_printed += i->second.size();
                    for (auto j : i->second) {
                        if (!quietMode) {
                            for (auto k : columnIndices) {
                                cout << database[tableName].table.at(j).at(k) << " ";
                            }//End inner for
                            cout << "\n";
                        }
                    }
                }
            }
            //GREATER THAN
            else if (opIn == Op::greaterThan) {
                auto upper = database[tableName].bst.upper_bound(condition);
                for (auto i = upper; i != database[tableName].bst.end(); i++) {
                    numRows_printed += i->second.size();
                    for (auto j : i->second) {
                        if (!quietMode) {
                            for (auto k : columnIndices) {
                                cout << database[tableName].table.at(j).at(k) << " ";
                            }//End inner for loop
                        cout << "\n";
                        }//End !quietMode
                    }
                }
            }
        }
        
        /***HASH && QUIET MODE***/
        else if (quietMode &&
                 !database[tableName].hash.empty() &&
                 database[tableName].hashCol == columnName &&
                 opIn == Op::equals) {
            
            auto found = database[tableName].hash.find(condition);
            if (found != database[tableName].hash.end()) {
                //NumRows_printed is equal to the numbere of items in found->second
                numRows_printed = found->second.size();
            }
        }
        
            
        //Else there isn't a hash on current table/column
        else {
            
            std::vector<size_t>rowVec;
                for (size_t i = 0; i < database[tableName].table.size(); i++) {
                    switch (opIn) {
                        case (Op::equals): {
                            if (database[tableName].table[i][colIndex] == condition) {
                                rowVec.emplace_back(i);
                            }
                            break;
                        }//End case Op::equals
                        case (Op::greaterThan): {
                            if (database[tableName].table[i][colIndex] > condition) {
                                rowVec.emplace_back(i);
                            }
                            break;
                        }//End case Op::greaterThan
                        case (Op::lessThan): {
                            if (database[tableName].table[i][colIndex] < condition) {
                                rowVec.emplace_back(i);
                            }
                            break;
                        }//End case Op::lessThan
                    }//End switch
                numRows_printed = rowVec.size();
            }
            
            if (!quietMode) {
                //For loop to print all the TableEntry items
                for (size_t i = 0; i < rowVec.size(); i++) {
                    for (size_t j = 0; j < columnIndices.size(); j++) {
                        cout << database[tableName].table[rowVec[i]][columnIndices[j]] << " ";
                    }
                    cout << "\n";
                }
            }
        }
    }
    cout << "Printed " << numRows_printed << " matching rows from " << tableName << std::endl;
    
}


//DELETE_FROM
void delete_from() {
    
    std::string tableName;
    std::string colName;
    std::string temp;
    char op;
    Op operatorIn;
    
    //Cin table name, the "WHERE", and the column name
    cin >> tableName >> temp >> colName;
    
    cin >> op;
    
    if (op == '<') {
        operatorIn = Op::lessThan;
    }
    else if (op == '=') {
        operatorIn = Op::equals;
    }
    else {
        operatorIn = Op::greaterThan;
    }
    
    //If table name is not found in database
    if (!tableFound(tableName)) {
        printTableError(tableName);
        return;
    }

    else {
        std::vector<std::string>::iterator found =
        std::find(database[tableName].colnames.begin(), database[tableName].colnames.end(), colName);
    
        int colIndex = int(std::distance(database[tableName].colnames.begin(), found));
        size_t startPos = database[tableName].table.size();
        
        if (found == database[tableName].colnames.end()) {
            cout << "Error: " << colName
            << " does not name a column in "
            << tableName << std::endl;
            getline(cin, colName);
            return;
        }
    
        else {
            
            database[tableName].table.erase(std::remove_if(database[tableName].table.begin(),
                                                             database[tableName].table.end(),
                                                             pred(operatorIn,
                                                                  returnTable(database[tableName].types[colIndex]),
                                                                  colIndex)),
                                                             database[tableName].table.end());
        }
        //Re-generate hash after deletion (if necessary)
        generate_on_col(tableName, colName);
        
        cout << "Deleted " << startPos - database[tableName].table.size()
        << " rows from " << tableName << std::endl;
        cin.clear();
    }
}


void generate(const std::string &tableNameIn) {
    std::string type;
    std::string temp;
    std::string colName;
    
    //cin >> hash/bst FOR COLUMN columnName
    cin >> type >> temp >> temp >> colName;
    
    database[tableNameIn].hashCol = colName;
    
    auto foundCol = std::find(database[tableNameIn].colnames.begin(), database[tableNameIn].colnames.end(), colName);
    //CHECK THIS
    size_t index = std::distance(database[tableNameIn].colnames.begin(), foundCol);
    
    
    if (foundCol == database[tableNameIn].colnames.end()) {
        cout << "Error: " << colName << " does not name a column in " << tableNameIn << std::endl;
        return;
    }
    else {
        
        //If current bst exists for table, clear
        if (!database[tableNameIn].bst.empty()) {
            database[tableNameIn].bst.clear();
        }
        //If hash table exists, clear
        if (!database[tableNameIn].hash.empty()) {
            database[tableNameIn].hash.clear();
        }
        
        //If index type is hash and hash table is currently empty
        if (type == "hash") {
            int hashIndex = 0;
            for (size_t i = 0; i < database[tableNameIn].table.size(); i++) {
                database[tableNameIn].hash[database[tableNameIn].table[i][index]].emplace_back(hashIndex);
                //entryIndex = index of TableEntry in table
                hashIndex++;
            }
        }
        
        //Type is bst
        else {
            int bstIndex = 0;
            for (size_t i = 0; i < database[tableNameIn].table.size(); i++) {
                database[tableNameIn].bst[database[tableNameIn].table[i][index]].emplace_back(bstIndex);
                bstIndex++;
            }
        }
        cout << "Created " << type << " index for table "
        << tableNameIn << " on column " << colName << std::endl;
    }
}

void generate_on_col(const std::string &tableIn, const std::string &colNameIn) {
    auto foundCol = std::find(database[tableIn].colnames.begin(), database[tableIn].colnames.end(), colNameIn);
    //CHECK THIS
    size_t index = std::distance(database[tableIn].colnames.begin(), foundCol);
    
    //If current bst exists for table, clear
    if (!database[tableIn].bst.empty()) {
        database[tableIn].bst.clear();

        int bstIndex = 0;
        for (size_t i = 0; i < database[tableIn].table.size(); i++) {
            database[tableIn].bst[database[tableIn].table[i][index]].emplace_back(bstIndex);
            bstIndex++;
        }
    }
    //If hash table exists, clear
    else if (!database[tableIn].hash.empty()) {
        int hashIndex = 0;
        database[tableIn].hash.clear();
        for (size_t i = 0; i < database[tableIn].table.size(); i++) {
            
            //No matter what, put back the index of TableEntry item
            //into corresponding hash TableEntry i key
            database[tableIn].hash[database[tableIn].table[i][index]].emplace_back(hashIndex);
            //entryIndex = index of TableEntry in table
            hashIndex++;
        }
    }
}

void join() {
    std::vector<std::string>printCols;
    std::vector<int>tableNum;
    std::string table1;
    std::string table2;
    std::string temp;
    std::string colname1;
    std::string colname2;
    int cols = 0;
    
    //cin  table1    AND     table2    WHERE    colname1    =      colname2
    cin >> table1 >> temp >> table2 >> temp >> colname1 >> temp >> colname2;
    //cin  AND     PRINT   num columns
    cin >> temp >> temp >> cols;
    
    //Reserve space for number of columns to be printed
    printCols.reserve(cols);
    tableNum.reserve(cols);
    
    auto foundCol1 = std::find(database[table1].colnames.begin(), database[table1].colnames.end(), colname1);
    auto foundCol2 = std::find(database[table2].colnames.begin(), database[table2].colnames.end(), colname2);
    
    //If colname1 isn't found in table1
    if (foundCol1 == database[table1].colnames.end()) {
        cout << "Error: " << colname1 << " does not name a column in " << table1 << std::endl;
        getline(cin, temp);
        return;
    }
    
    //If colname2 isn't found in table2
    else if (foundCol2 == database[table2].colnames.end()) {
        cout << "Error: " << colname2 << " does not name a column in " << table2 << std::endl;
        getline(cin, temp);
        return;
    }
    
    //Handles Error 1 (if table is not found in database)
    else if (!tableFound(table1) || !tableFound(table2)) {
        if (!tableFound(table1)) {
            printTableError(table1);
        }
        else {
            printTableError(table2);
        }
        //Gets rest of instruction line
        getline(cin, temp);
        return;
    }
    
    //If table is in database
    else {
        std::unordered_map<TableEntry, std::vector<int>>table2_map;
        std::string printCol;
        std::string currentTable;
        int table = 0;
        int numRows = 0;
        
        //Cin the columns to be printed and their respective table
        //push back into vectors
        for (int i = 0; i < cols; i++) {
            cin >> printCol >> table;
            
            switch(table) {
                case (1): currentTable = table1;
                    break;
                case (2): currentTable = table2;
                    break;
            }
            auto foundCol = std::find(database[currentTable].colnames.begin(),
                                      database[currentTable].colnames.end(),
                                      printCol);
            
            //If column is not found in the corresponding table
            if (foundCol == database[currentTable].colnames.end()) {
                cout << "Error: " << printCol << " does not name a column in " << currentTable << std::endl;
                getline(cin, printCol);
                return;
            }
            //Else push back column and corresponding table (1|2) into vectors
            else {
                printCols.push_back(printCol);
                tableNum.push_back(table);
            }
        }
        
        //Get index of second column name in table2
        size_t index2 = std::distance(database[table2].colnames.begin(), foundCol2);

        //Generate local BST on table2
        int bstIndex = 0;
        for (size_t i = 0; i < database[table2].table.size(); i++) {
            table2_map[database[table2].table[i][index2]].emplace_back(bstIndex);
            bstIndex++;
        }
        
        if (!quietMode) {
            for (size_t i = 0; i < printCols.size();i++) {
                cout << printCols[i] << " ";
            }
            cout << "\n";
        }

        //Loop through rows of table1, find elements that also exist in table2
        for (size_t i = 0; i < database[table1].table.size(); i++) {
            auto found = table2_map.find((database[table1].table[i][database[table1].colIndex[colname1]]));
            
            if (found != table2_map.end()) {
                //Go through each element of found->second (vector of ints)
                for (auto k : found->second) {
                    if (!quietMode) {
                        for (size_t j = 0; j < printCols.size(); j++) {
                            if (tableNum[j] == 1) {
                                //Index of printCols[j] in table 1
                                int index = database[table1].colIndex[printCols[j]];
                                cout << database[table1].table[i][index] << " ";
                            
                            }
                            else {
                                int index = database[table2].colIndex[printCols[j]];
                                cout << database[table2].table.at(k).at(index) << " ";
                            
                            }//End of else
                        }//End of inner for
                    }//End of !quietMode
                    if (!quietMode) cout << "\n";
                    ++numRows;
                }//End of mid for
            }//End of if (found != table2_map.end())
        }//End of outer for loop
        cout << "Printed " << numRows << " rows from joining " << table1 << " to " << table2 << "\n";
    }
}

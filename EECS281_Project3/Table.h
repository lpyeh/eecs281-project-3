//
//  Table.h
//  EECS281_Project3
//
//  Created by Leigh Yeh on 3/9/18.
//  Copyright © 2018 Leigh Yeh. All rights reserved.
//
#include <string>
#include <iostream>
#include <vector>
#include "TableEntry.h"
#include <map>
#include <unordered_map>

using std::cin;
using std::cout;
using Row = std::vector<TableEntry>;

class Table {
public:
    
    enum class Type {String, Double, Int, Bool};
    
    Table();
    
    Table(int numCols);
    
    int numCols;
    
    std::string hashCol;

    std::vector<Row>table;
    
    //Vector to hold types of columns
    std::vector<Type>types;
    
    //Vector to hold column names
    std::vector<std::string>colnames;
    
    std::map<std::string, int>colIndex;
    
    std::unordered_map<TableEntry, std::vector<int>>hash;
    
    std::map<TableEntry, std::vector<int>, std::less<TableEntry>>bst;
};



//
//  Table.cpp
//  EECS281_Project3
//
//  Created by Leigh Yeh on 3/9/18.
//  Copyright © 2018 Leigh Yeh. All rights reserved.
//

#include <stdio.h>
#include "Table.h"
#include "TableEntry.h"

//Default ctor initialize list
Table::Table() : numCols(0), hashCol(""), types(0), colnames(0), hash(0) {}

Table::Table(int colIn) : numCols(colIn), hashCol(""), types(0), colnames(0), hash(0) {}
